from django.shortcuts import render
from program.models import Program

# Create your views here.
def index(request):
    program_list = list(Program.objects.all())
    return render(request, 'news.html', {'programs0': program_list[0:3], 'programs1': program_list[3:6]})