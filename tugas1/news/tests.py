from django.test import TestCase, Client

# Create your tests here.
class NewsTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_if_url_exist(self):
        response = self.client.get('/news/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'news.html')