from django.urls import path
from .views import index, add_testimoni, get_name

app_name = 'testimoni'

urlpatterns = [
    path('', index, name='index'),
    path('add-testimoni', add_testimoni, name='add-testimoni'),
    path('get-name', get_name, name='get-name'),
]