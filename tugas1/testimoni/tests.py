from django.test import TestCase, Client

# Create your tests here.
class TestimoniTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_exist(self):
        resp = self.client.get('/testimoni/')
        self.assertEqual(resp.status_code, 200)