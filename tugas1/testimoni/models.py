from django.core.validators import MaxLengthValidator
from django.db import models


# Create your models here.

class Testimoni(models.Model):
    testimoni = models.CharField(max_length=300, validators=[MaxLengthValidator(300)])
    created_by = models.ForeignKey('donate.UserModel', related_name='testimoni', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
