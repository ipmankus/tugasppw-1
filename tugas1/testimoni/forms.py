from django import forms
from .models import Testimoni

class TestimoniForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(TestimoniForm, self).__init__(*args, **kwargs)

        self.fields['testimoni'].label = 'Testimoni'
        self.fields['testimoni'].widget = forms.Textarea(attrs={'class': 'form-control',
                                                             'placeholder': 'Apa yang anda rasakan?',
                                                             'rows': 6,
                                                             'cols': 7,
                                                             'style': 'resize : none'}
                                                      )
        self.fields['testimoni'].error_messages = {'max_length': 'Maksimal 300 karakter :(',
                                                'required': 'Mohon mengisi testimoni'}

    class Meta:
        model = Testimoni
        fields = ['testimoni']