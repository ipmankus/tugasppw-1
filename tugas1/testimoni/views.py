from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse

from .forms import TestimoniForm
from .models import Testimoni
from donate.models import UserModel
# Create your views here.

def index(request):
    testimoni = Testimoni.objects.all()
    form = TestimoniForm()
    html = 'testimoni.html'
    return render(request, html, {'status': testimoni, 'form': form})


def add_testimoni(request):
    if request.method == 'POST':
        print(request.POST)
        form = TestimoniForm(request.POST)
        if form.is_valid():
            testimoni = form.save(commit=False)
            user = UserModel.objects.get(email=request.session['email'])
            testimoni.created_by = user
            testimoni.save()
            return HttpResponseRedirect(reverse('testimoni:index'))
    else:
        form = TestimoniForm()

    return render(request, 'testimoni.html', {'status' : Testimoni.objects.all(), 'form' : form})


def get_name(request):
    name = Testimoni.objects.all()
    name_list = [{'id': obj.id, 'name': obj.name, 'email': obj.email}
                        for obj in subscribers]
    return HttpResponse(json.dumps({'subscribers': name_list}),
                        content_type='application/json')