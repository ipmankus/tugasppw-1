from django.shortcuts import render
from django.http import HttpResponse
from donate.models import UserModel

# Create your views here.
def user_profile(request):
    if request.user.is_authenticated:
        user = UserModel.objects.get(email=request.session['email'])
        ctx = {'donated': user.donator_set.all()}
        return render(request, 'profile.html')
    else:
        return render(request, 'profile.html')
