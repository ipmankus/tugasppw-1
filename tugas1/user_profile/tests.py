from django.test import TestCase, Client
from django.urls import reverse


# Create your tests here.
class UserProfileTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_if_url_exist(self):
        response = self.client.get('/profile/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'profile.html')
