from django import forms
from .models import UserModel

class DateInput(forms.DateInput):
    input_type = 'date'


class UserCreateForm(forms.ModelForm):
    class Meta:
        model = UserModel
        fields = ['username', 'password', 'first_name', 'last_name', 'email', 'birthday']
        widgets = {
            'birthday': DateInput(attrs={'type': 'date', 'class': 'form-control'}),
            'password': forms.PasswordInput(attrs={'type': 'password', 'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'type': 'text', 'class': 'form-control'}),
            'first_name': forms.TextInput(attrs={'type': 'text', 'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'type': 'text', 'class': 'form-control'}),
            'username': forms.TextInput(attrs={'type': 'text', 'class': 'form-control'}),
        }