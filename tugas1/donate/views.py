from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login
from donate.models import UserModel
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, JsonResponse
from .forms import UserCreateForm
from program.models import Program, Donator

# Create your views here.
def index(request):
    if request.user.is_authenticated:
        return redirect('/news')
    if(request.POST):
        form = UserCreateForm(request.POST)
        if form.is_valid():
            user = form.save()
            request.session['display_name'] = user.username
            request.session['email'] = user.email
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            return redirect('be-a-donator')
        else:
            return render(request, 'bea.html', {'form': form, 'status': 'not valid!'})
    form = UserCreateForm()
    return render(request, 'bea.html', {'form': form})

def list_donation(request):
    if not request.user.is_authenticated:
        return redirect('/home')

    if request.user.is_authenticated:
        user = get_object_or_404(UserModel, email=request.session['email'])
        donators = list(user.donator_set.all())

        return render(request, 'donation_list.html', {'donators': donators})

def get_list_donation(request):
    if not request.user.is_authenticated:
        return JsonResponse({'result': 'User not authenticated'})

    if request.user.is_authenticated:
        user = get_object_or_404(UserModel, email=request.session['email'])
        donators = [{'name': x.donator_name, 'jumlah': x.donation_amount} for x in user.donator_set.all()]

        return JsonResponse({'donators': donators})