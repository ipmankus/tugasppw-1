from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class UserModel(User):
    birthday=models.DateField(auto_now=False, null=True, blank=True)