from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client


class DonateTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_if_url_exist(self):
        response = self.client.get('/be-a-donator/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'bea.html')