from django.test import TestCase, Client
from django.urls import reverse


# Create your tests here.
class SilverLightTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_if_url_exist(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')


