from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from donate.models import UserModel


# Create your views here.
def index(request):
    return render(request, 'home.html')

def set_extra_data_on_session(backend, strategy, details, response,
    user=None, *args, **kwargs):
    if backend.name == 'google-oauth2':
        strategy.session_set('id_token', response['id_token'])
        strategy.session_set('email', response['emails'][0].get('value'))
        strategy.session_set('display_name', response['displayName'])
        strategy.session_set('image_url', response['image'].get('url')[:-2] + '25')
    UserModel.objects.get_or_create(
        username=response['displayName'],
        first_name=response['displayName'],
        last_name=response['displayName'],
        email=response['emails'][0].get('value'),
        birthday='1970-01-01',
        password='-',
    )

def testajax(request):
    return JsonResponse(request.COOKIES);