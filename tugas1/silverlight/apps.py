from django.apps import AppConfig


class SilverlightConfig(AppConfig):
    name = 'silverlight'
