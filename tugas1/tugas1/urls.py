"""tugas1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views
from django.urls import path, include
from donate.views import list_donation, get_list_donation
import silverlight

urlpatterns = [
    path('', include('silverlight.urls')),
    path('home/', include('silverlight.urls'), name='home'),
    path('admin/', admin.site.urls),
    path('be-a-donator/', include('donate.urls')),
    path('list-donation/', list_donation, name='list_donation'),
    path('get-list-donation/', get_list_donation, name='get_list_donation'),
    path('test/', silverlight.views.testajax, name='testajax'),
    path('news/', include('news.urls')),
    path('program/', include('program.urls')),
    path('profile/', include('user_profile.urls')),
    path('logout/', views.LogoutView.as_view(next_page='/home/'), name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),
    path('testimoni/', include('testimoni.urls', namespace='testimoni')),
]
