from django.shortcuts import render, redirect
from .models import Program, Donator
from donate.models import UserModel
from .forms import DonateForm
from django.shortcuts import get_object_or_404


def get_program(request, program_id):
	program = get_object_or_404(Program, id=program_id)
	donators = list(program.donator_set.all())
	form = DonateForm()
	percent_funded = str(int(min((program.donation_funded / program.donation_goal)* 100, 100)))
	if(request.POST):
		form = DonateForm(request.POST)
		user = get_object_or_404(UserModel, email=request.session['email'])
		if form.is_valid():
			donator = form.save(commit=False)
			donator.program = program
			donator.user = user
			donator.donator_name = request.session['display_name']
			program.donation_funded += donator.donation_amount
			program.save()
			donator.save()
		return redirect('/program/' + str(program_id))

	return render(request, 'donate.html', {'program': program, 'form': form,
		'donators': donators, 'percent_funded': percent_funded})
