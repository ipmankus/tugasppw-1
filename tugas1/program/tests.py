from django.test import TestCase, Client
from .models import Program
from datetime import datetime
from donate.models import UserModel
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

class test_model_program(TestCase):
	def setUp(self):
		self.client = Client()
		self.super_user = User.objects.create_superuser('ikhsan', 'sysadmin@skip.com', '*********skip******!23')


	def test_if_url_does_not_exists(self):
		response = self.client.get('/program/1')
		self.assertEqual(response.status_code, 404)


	def test_post_donation(self):
		p = Program(program_name = "Gempa Lombok",
					program_description = "Gempa terjadi di lombok. Mereka membutuhkan donasimu segera!",
					program_image_url = "https://google.com",
					donation_funded = 0.0,
					donation_goal = 5000000.0,
					donation_start_date = datetime(2018, 9, 10),
					donation_end_date = datetime(2018, 11, 10))
		p.save()
		self.client.post('/be-a-donator/', data={
			'username':'sanul',
			'first_name':'san',
			'last_name':'san',
			'email':'un@ique.com',
			'birthday':'1970-01-01',
        	'password':'-'
		})
		response = self.client.post('/program/1', data={'donation_amount':'2', 'show_name':'true'})
		self.assertEqual(response.status_code, 302)


	def test_if_url_exists(self):
		p = Program(program_name = "Gempa Lombok",
					program_description = "Gempa terjadi di lombok. Mereka membutuhkan donasimu segera!",
					program_image_url = "https://google.com",
					donation_funded = 0.0,
					donation_goal = 5000000.0,
					donation_start_date = datetime(2018, 9, 10),
					donation_end_date = datetime(2018, 11, 10))

		p.save()
		response = self.client.get('/program/1')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed('donate.html')


	def test_create_program(self):
		p = Program(program_name = "Gempa Lombok",
					program_description = "Gempa terjadi di lombok. Mereka membutuhkan donasimu segera!",
					program_image_url = "https://google.com",
					donation_funded = 0.0,
					donation_goal = 5000000.0,
					donation_start_date = datetime(2018, 9, 10),
					donation_end_date = datetime(2018, 11, 10))

		p.save()
		self.assertTrue(Program.objects.all())