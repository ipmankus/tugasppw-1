from django.urls import path
from program.views import  get_program

urlpatterns = [
    path('<int:program_id>', get_program)
]