from django import forms
from .models import Program, Donator
from django.core.exceptions import ValidationError


class DonateForm(forms.ModelForm):
    class Meta:
        model = Donator
        fields = ['donation_amount',  'show_name']

    def clean_donation_amount(self):
        donation_amount = self.cleaned_data['donation_amount']

        if donation_amount < 1:
            raise ValidationError('Minimum Donation $1')
        return donation_amount

    def clean(self):
        show_name = self.cleaned_data['show_name']
        if not show_name:
            self.cleaned_data['donator_name'] = 'Anon'
        return self.cleaned_data