from django.db import models
from django.utils import timezone


class Program(models.Model):
	id = models.AutoField(primary_key=True)
	program_name = models.CharField(max_length=30)
	program_description = models.CharField(default="No description.", max_length=1000)
	program_image_url = models.URLField(default=None)
	donation_funded = models.FloatField()
	donation_goal = models.FloatField()
	donation_start_date = models.DateField()
	donation_end_date = models.DateField()
	date_added = models.DateTimeField(default=timezone.now)


class Donator(models.Model):
	donator_name = models.CharField(max_length=30)
	donation_amount = models.FloatField()
	show_name = models.BooleanField(default=True)
	program = models.ForeignKey('Program', on_delete=models.CASCADE)
	user = models.ForeignKey('donate.UserModel', on_delete=models.CASCADE, null=True)
	donate_date = models.DateTimeField(default=timezone.now)